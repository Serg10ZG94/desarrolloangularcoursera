import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesEffects, DestinosViajesState, initializeDestinosViajesState, reducerDestinosViajes } from './models/destinos-viajes-state.model';
import { ActionReducerMap, StoreModule as NgrxStoreModule } from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {environment} from "../environments/environment"; //ANGULAR CLI Environment

const routes: Routes=[
  {path:"", redirectTo: "home", pathMatch:"full"},
  {path:"home", component:ListaDestinosComponent},
  {path:"destino/:id", component:DestinoDetalleComponent}
];

// INIT REDUX

export interface AppState{
  destinos:DestinosViajesState;
}

const reducers : ActionReducerMap<AppState>={
  destinos:reducerDestinosViajes
};

const reducersInitialState={
  destinos:initializeDestinosViajesState()
};

// FIN INIT REDUX
  
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgrxStoreModule.forRoot(reducers,{initialState:reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge:25,//Retains last 25 states
      logOnly:environment.production,//Restrict extension to log-only mode
    })
  ],
  providers: [
    DestinosApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
