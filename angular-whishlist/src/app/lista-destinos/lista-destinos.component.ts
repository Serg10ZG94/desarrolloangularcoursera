import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppState } from '../app.module';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import {Store} from "@ngrx/store";
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates:string[];
  all;

  constructor(private destinosApiClient:DestinosApiClient,private store:Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = String[];
    this.store.select(state=>state.destinos.favorito)
    .subscribe(d=>{
      if(d != null){
        this.updates.push('se ha elegido a' + d.nombre)
    }
    });
    store.select(state=>state.destinos.items).subscribe(items=>this.all=items);
    /* this.destinosApiClient.subscribeOnChange((d:DestinoViaje)=>{
      if(d != null){
          this.updates.push('se ha elegido a' + d.nombre)
      }
    }); */
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

}