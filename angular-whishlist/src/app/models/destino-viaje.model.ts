import { ObjectUnsubscribedError } from 'rxjs';
import {v4 as uuid} from "uuid";

export class DestinoViaje {
    selected: boolean;
    servicios:string[];
    id=uuid();

    constructor(public nombre: string, public u:string,public votes:number =0){
        this.servicios=['alberca','desayuno'];
    }
    setSelected(s:boolean){
        this.selected=s;
    }

    isSelected():boolean{
        return this.selected;
    }

    voteUp():any{
        this.votes++;
    }

    voteDown():any{
        this.votes--;
    }
}